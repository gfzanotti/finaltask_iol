# FinalTask_IOL
Zanotti Gianfranco     matricola: 813863  

### TITOLO : "valutazione comparativa di approcci per il calcolo delle Class Attention Maps (CAMs) in reti neurali convolutive"

##### Premesse

Questo progetto richiede la conoscenza di nozioni non facenti parte del corso di studi, per colmare questa lacuna ho preso a riferimento i corsi di **DEEP LEARNING** della *Stanford University*.

Come risorsa hardware ho utilizzato una workstation del 2013\2014 con una GPU Nvidia Quadro K3100M con 4 GB di ram. Per ottenere qualche risultato ho dovuto selezionare molto attentamente le immagini prediligendo immagini "ICONICHE" e cercando di scartare immagini "DI CONTESTO" che possono presentare più oggetti ma soprattutto oggetti di più classi, ed i dataset scelti essendo nati per la object classification mi hanno creato qualche difficoltà.

Sono comunque riuscito ad ottenere dataset con qualche migliaio di immagini e 4 classi che ho utilizzato per questo compito.

------

 

### DESCRIZIONE PROGETTO PROVA FINALE

(cercherò di essere didascalico)



#### REQUISITI

Questo progetto tratta un problema di CLASSIFICAZIONE DI IMMAGINI 

Linguaggio usato per il progetto : PYTHON (kernel ver. 3.7.7)

* Dati 2 dataset di immagini (tra i più utilizzati nelle competizioni di Computer Vision), estrarre un sotto-insieme di immagini ad hoc per il task di "CLASSIFICAZIONE DI IMMAGINI". Le immagini scelte dovranno corrisponde  ad immagini simboliche (iconiche) anzichè di contesto, anche per agevolare l'addestramento delle reti, data la limitazione dei mezzi computazionali. I dataset di riferimento sono Pascal-VOC e Microsoft-COCO.

  Verranno formati 3 insiemi di immagini per il train, la validazione ed il test. Tutte le immagini dovranno essere supervisionate (presenza dei rispettivi file di annotazione)

* Dalla bibliografia di riferimento selezionare le Reti Neurali Convoluzionali (CNNs) da addestrare ed effettuare le opportune modifiche di struttura per l'applicazione degli algoritmi di Class Activation Mapping.

* Addestramento e validazione delle CNNs sui dataset opportunamente creati. Si usano i set di train e validation.

* Testing sulle CNNs addestrate per valutare la robustezza delle metriche ottenute in validazione; si usa il set di test.

* Estrazione delle superfici, nelle singole immagini, che le CNNs usano per la classificazione: HEATMAP. Tali superfici sono ottenute con gli algoritmi di Class Activation Map (CAM) e Gradient Class Activation Map (GradCAM).

* Applicazione di metodi per il Boundig Boxing sulle heatmap

* Confronto qualitativo tra i Bounding Boxing estratti con le CNNs e quelli supervisionati.

* Applicazione di una metrica di confronto: Intersection Over Union (IoU)



**Suddivido il progetto in 3 sezioni:**

1. Recupero dei dataset delle immagini dai loro repository  ufficiali (Pascal_VOC e COCO) e creazione del dataset da utilizzare in questo task. I miei dataset sono composti da 4 classi e 2920 immagini il Pascal_VOC mentre COCO ne contiene 1480, equamente distribuiti tra le classi. 

   

2. **file Jupyter Notebook** di definizione delle reti neuali convoluzionali (CNNs), training, validazione e test. Reti scelte: VGG16 e Alexnet (quest'ultima *ancora non sono riuscito ad addestrarla*). Ho scelto di mantenere i file in formato Jupyter Notebook per la continua necessità di cambiare parti di codice (parametri per l'addestramento). 

   

3. valutazione qualitativa delle tecniche di Class Activation Mapping vs identificazione supervisionata degli oggetti (Bounding Box), La metrica utilizzata per confrontare i 2 tipi di bonding box (quello supervisionato e quello estratto delle HEATMAPS) é l' IoU (Intersection over Union)

   

   **<u>N.B:  utilizzo i Notebook di Jupyter come interfaccia grafica per eseguire il codice ed eventualmente corredarlo con spiegazioni</u>**

   ------

   

#### PARTE 1: recupero dei dataset dai loro repository (Pascal_VOC e COCO) e costruzione del dataset ad hoc per questo task

Dati i problemi dovuti all'addestramento delle reti con dataset di notevoli dimensioni e complessità per cui non ottenevo risultati "decenti", ho scelto di formare datasets con immagini il più possibile iconiche (con uno o pochi oggetti di una sola classe presenti) a scapito di immagini di contesto.

Ho iniziato dal dataset più piccolo: **Pascal_VOC** con 22.263 immagini supervisionate (accompagnate da singoli file XML di annotazione) ,suddivise in 20 CLASSI di oggetti. 

Ho selezionato, attraverso i file di annotazione quelle immagini che contenevano 1 sola classe di oggetti ed a questo punto ho scelto le classi che contenevano almeno 730 immagini (che sono 4). Le classi sono : <u>bird</u>, <u>cat</u>, <u>dog</u>, <u>person</u>.

Ho copiato le 730 immagini per le 4 classi in directory "ad hoc" per essere usate come ingresso delle reti neurali. Sono state divise in 2 set; <u>uno di train e validation</u> ed <u>uno di test</u> con 80 elementi per classe

Per il dataset **COCO** ho selezionato le immagini per le classi già scelte ed ho verificato che non contenessero oggetti di altre classi, ottenendo 370 immagini per classe.

Tutte queste immagini sono supervisionate: sono accompagnate da file di annotazioni che descrivono l'immagine e ciò che contiene. Al mio scopo occorrono i dati che definiscono la posizione degli oggetti, i "bounding box" che forniscono i 2 vertici opposti di un rettangolo che contiene l'oggetto. Queste informazioni servono per confrontare come il PC ed il SUPERVISORE hanno scelto l'area dell'immagine che ne definisce la CLASSIFICAZIONE.

Per la *procedura di selezione delle immagini*, come detto, vengono lette le informazini sui file di annotazioni.

<u>Pascal VOC</u> usa file di annotazioni, per ogni singola immagine, in formato XML con una struttura ad albero dei TAG. Con l'uso della libreria [`xml.etree.ElementTree`](https://docs.python.org/3.7/library/xml.etree.elementtree.html#module-xml.etree.ElementTree) viene creato un oggetto di classe ElementTree che fornisce il supporto per la serializzazione dallo standard XML. Tramite i metodi della libreria vengono parsati i file di annotazioni alla ricerca del TAG <name> che contiene la definizione della classe dell'oggetto ma può contenere anche al descrizione delle parti di cui un oggetto é composto.

Confronto il valore del TAG <name> con le chiavi di un dizionario creato dalla lista dei nomi delle classi escludendo così quei TAG <name> che si riferiscono a parti di un oggetto. Conto le occorrenze di degli oggetti nei file e le memorizzo in una lista "lines" che aggiungo poi al file CSV memorizzato.

Utilizzando i dati inseriti nel file CSV ho preso le 4 classi che presentavano il maggior numero di immagini di tipo "iconico" (con una sola classe di oggetti al loro interno). Queste 4 classi (che sono: <u>bird</u>, <u>cat</u>, <u>dog</u>, <u>person</u>) contengono più di 730 immagini; ne ho prese 730 esatte per classe per creare dataset *bilanciati*.

ESEMPIO DI UN FILE XML di annotazioni:



![annotationXML](./img/annotationXML.jpg)

Per visualizzare la sequenza dei metodi usati e la distribuzione degli oggetti rimando al relativo [Sequence Diagram](./UML/SequenceDiagDataset.jpg)

*Sequence Diagram per la selezione sul dataset Pascal VOC:*

![Sequence Diagram dsVOC](./UML/SequenceDiagDataset.jpg)



<u>COCO</u> usa un singolo file di annotazioni per set di immagini (ad esempio quello del set train del 2017 ha dimensione di oltre 458 MB); ed usa il formato Json per la memorizzazione dei dati ( REFERENCE: https://cocodataset.org/#format-data). Ho utilizzato la libreria **pycocotools** che contiene i metodi per l'estrazione dei dati da questi file.

Ho estratto solo le immagini relative alle classi scelte con Pascal VOC e con le caratteristiche di rappresentazione degli oggetti descritte sopra.

Per  l'elenco esaustivo delle classi e metodi usati rimando al [Class Diagram](./UML/ClassDiagramDataset.jpg)

*Class Diagram per il package Dataset*:

![Class Diagram per il package Dataset](./UML/ClassDiagramDataset.jpg)



Nei files formato <u>Notebook di Jupyte</u>r presenti nella cartella [./Notebook/1_Datasets](Notebook/1_Datasets) , che utilizzo come intefaccia grafica per l'esecuzione del codice, descrivo i metodi ed il loo scopo.

------



#### PARTE 2: definizione, addestramento e test delle CNNs

I notebook di addestramento sono 4 (2 reti per 2 datsets) ma in sostanza hanno la stessa struttura:

- cambia la sorgente dei dati e il tipo di rete
- cambiano i settaggi degli iperparametri

In questa fase, dopo vari approcci con librerie e paradigmi differenti, ho scelto di utilizzare **Tensorflow 2.0**.

La scelta in realtà è stata indirizzata univocamente dal fatto che l'unica GPU che potevo utilizzare ha una Compute Capability (CC) dell'architettura CUDA di 3.0; per tale motivo, ad esempio, non sono riuscito ad utilizzare la libreria PyTorch le cui attuali versioni richiedono una CC superiore al 6! Non sono riuscito nemmeno a compilare i sorgenti del driver PyTorch per la mia GPU



In questa fase ho eseguito i seguenti passi uniformandomi al tutorial ufficiale sull'addestramento delle CNNs di Tensorflow: https://www.tensorflow.org/tutorials/images/classification.

1. caricamento ed eventuale visualizzazione dei dati (directory di immagini). Carico le immagini da 1 sola directory e faccio poi una "*validation split*".

   Utilizzo la classe `ImageDatagenerator` alla quale passo i parametri `rescale` e `validation_split`.  Chiamo poi il metodo `flow_from_directory` che crea i batch che serviranno da inputs per la CNN.

2. definisco i parametri di regolarizzazione attraverso la classe `tf.keras.regularizers.L1L2`

3. creo una istanza di CNN con la classe `tensorflow.keras.applications.vgg16`(ad esempio). Eseguo poi eventuali modifiche sull'istanza di CNN appena creata, esempio modifica dei layers, attraverso gli attributi `layers`

4. compilo ed eseguo l'addestamento del modello attraverso i metodi di classe `classModel.compile(`) e `classModel.fit()` 

   Ciò che ottengo dopo la fase di addestramento è schematizzabile in questi grafici: 

   

   ![grafico parametri training CNN](./img/graficiTrainCnn.jpg)

   

   Il primo grafico mostra l'andamento della metrica di ***Accuracy*** (accuratezza) ad ogni ciclo di addestramento della rete (*EPOCA*) con aggiornamento dei "pesi di ogni layer della CNN". In ordinata è posto il valore dell'accuratezza definita come
   $$
   Accuracy = \frac{n°\ immagini\  classificate\ correttamente}{n°\ totale\ di\ immagini\ classificate}
   $$
   

   Questi valori sono soggetti a fluttuazione statistica, diverse procedure di addestramento con gli stessi iperparametri possono dare valori simili ma non uguali.

   Il secondo grafico mostra l'andamento del valore della funzione di ***LOSS*** durante le EPOCHE di addestramento, l'obiettivo della rete neurale è di minimizzare tale funzione.

   In questo caso la funzione di LOSS scelta è stata la **categorical cross entropy**. 

   Data al funzione "**Softmax**" come funzione di attivazione dell'ultimo layer, così definita: 
   $$
   f(s)_i = \frac{e^{s_i}}{\sum^C_j{e^{s_j}}}
   $$
   dove con s indico i valori di predizione per le singole classi estratti dalla CNN, la **categorical cross entropy (CE)** si definisce come:
   $$
   CE = -\sum^C_i t_ilog(\frac{e^{s_i}}{\sum^C_j{e^{s_j}}})
   $$
   nella fattispecie t<sub>i</sub> è uguale a 1 per la classe individuata dalla CNN e 0 per tutte le altre classi, per cui

   
   $$
   CE = -log(\frac{e^{s_p}}{\sum^C_j{e^{s_j}}})
   $$
   
5. Infine applico delle tecniche di contrasto dell'overfitting come la <u>Data Augmantation</u> ed il <u>Dropout</u>.

   I parametri da tenere in considerazione e da provare a modificare sono tanti per questo motivo mi è stato comodo mantenere questa parte di codice nel formato dei Notebook di Jupyter.
   



Nella seguente tabella mostro i risultati ottenuti, in termini delle metriche Acccuracy e Loss, nelle fasi di addestramento e test delle reti.

| CNNs                                                         | TRAIN accuracy(loss) | VALIDATION accuracy(loss) | TEST accuracy(loss) |
| ------------------------------------------------------------ | -------------------- | ------------------------- | ------------------- |
| VggCamVoc                                      (VOC_20200818-172023_model.h5) | 0.9899 (6.93)        | 0.9135 (7.8)              | 0.9219 (32.64)      |
| VggCamCoco                                 (COCO_20200731-140659_model.h5) | 0.9975 (4.54)        | 0.8446 (5.09)             | 0.8531 (20.21)      |
| VggVoc                                               (VOC_20200819-110644_model.h5) | 0.9840 (0.054)       | 0.8892 (0.31)             | 0.8938 (338.64)     |
| VggCoco                                          (COCO_20200823-104504_model.h5) | 1 (0.029)            | 0.8426 (0.46)             | 0.8281 (338.31)     |

------



#### PARTE 3: inferenza (come le CNNs scelgono la classe e confronto con la supervisione)

Utilizzando i file delle reti addestrate viene eseguita una predizione su una immagine.

Applicando gli algoritmi di estrazione delle mappe di attivazione (**CAM** e **GradCAM**) sulla predizione e sugli strati intermedi della CNN si ottiene una mappa dell'immagine con le parti che la rete CNN ritiene piu o meno significative per la predizione della data classe.

Sovrappongo la mappa di attivazione (**Heatmap**) all'immagine originale.

Calcolo sulla mappa di attivazione l'area più significativa e la delimito con un **Bounding Box**,

Estrapolo i Bounding Box "supervisionati" dai file di annotazioni.

Calcolo l'Intersection over Union (IoU)

Alcuni esempi di valori calcolati :

| FILE                   | IoU CAM (VOC) | IoU GradCAM (VOC) | IoU CAM (Coco) | IoU GradCAM (Coco) |
| ---------------------- | ------------- | ----------------- | -------------- | ------------------ |
| person/2008_002538.jpg | 0.4750        | 0.2435            | 0.4948         | 0.2796             |
| person/2008_002561.jpg | 0.2092        | 0.1048            | 0.3729         | 0.1017             |
| bird/2011_000912       | 0.0202        | 0.0432            | 0.0636         | 0                  |
| bird/2011_000222       | 0.5552        | 0.5267            | 0.5466         | 0.3344             |
| cat/2010_003758        | 0.3052        | 0.1718            | 0.3818         | 0.4512             |
| dog/2010_004339        |               | 0.1146            |                | 0.0483             |

Nei files formato <u>Notebook di Jupyte</u>r presenti nella cartella [./Notebook/3_CAMs](Notebook/3_CAMs) , che utilizzo come intefaccia grafica per l'esecuzione del codice, descrivo i metodi ed il loro scopo.

Per  l'elenco esaustivo delle classi e metodi usati rimando al [Class Diagram](./UML/ClassDiagramHeatmap.jpg), lo si può notare anche nei Notebook di esecuzione del programma.

*Class Diagram per il package Heatmap*:

![*Class Diagram per il package Heatmap*](./UML/ClassDiagramHeatmap.jpg)





*Il Sequence Diagram per la produzione della heatmap sulla classe HeatmapCam:*

![Il Sequence Diagram per la produzione della heatmap sulla classe HeatmapCam:](./UML/SequenceDiagHeatmap.jpg)



##### SUITE DI TEST

Infine nella cartella ./FinalTask_IOL/Test ho organizzato alcuni file per il testing sulle classi del progetto.



##### Bibliografia



METHODS TO COMPARE

- Original CAM https://arxiv.org/pdf/1512.04150.pdf
- Grad Cam: https://arxiv.org/abs/1610.02391





Data Sets

- Pascal VOC http://host.robots.ox.ac.uk/pascal/VOC/
- MS coco [http://cocodataset.org/](http://cocodataset.org/#home)
