from src.dataset.dataset import Dataset
import xml.etree.ElementTree as ET

import os
import csv


class DatasetVoc(Dataset):
    listClass = ["aeroplane", "bicycle", "bird", "boat", "bottle", "bus", "car", "cat", "chair", "cow", "diningtable",
                 "dog", "horse", "motorbike", "person", "pottedplant", "sheep", "sofa", "train", "tvmonitor"]
    listClass.insert(0, 'Image_Id')     # in VOC Image_Id => FILE NAME
    listClass.insert(len(listClass), 'Sum_Obj')

    def main(self):
        pass

    # COSTRUTTORE
    def __init__(self, annotationPath, imagesPath, destDir):
        super().__init__(annotationPath, imagesPath, destDir)

    # parsa tutti i file XML delle annotazioni e chiama "xml2csv"
    def createCsv(self):
        try:
            os.remove('./Notebook/1_Datasets/CSV/dataVOC.csv')
        except IOError:
            print("File dataVOC.csv non cancellato: non esiste")

        files = [file for file in os.listdir(self.annotation_path)]

        with open('./Notebook/1_Datasets/CSV/dataVOC.csv', 'a', newline='') as writeFile:
            writer = csv.writer(writeFile)

            # aggiungo la riga di intestazione
            writer.writerow(self.listClass)

        counter = 0
        for file in files:
            _file = os.path.join(self.annotation_path, file)
            self.xml2csv(_file)

            # conto i file  che vengono parsati
            counter += 1
        print(counter)

    def xml2csv(self, file):

        # ATTENZIONE _dict deve essere riazzerato alla lettura di ogni file
        _dict = dict([(_class, 0) for _class in self.listClass])

        tree = ET.parse(file)
        root = tree.getroot()

        lines = []
        for i in range(22):
            lines.append(0)  # inizializzo la riga, da aggiungere, con tutti 0
            # print(lines)

        # ottengo il prefisso dei file *.jpg
        prefix = os.path.basename(os.path.splitext(file)[0])

        # metto il nome file nella prima colonna
        lines[0] = prefix + ".jpg"

        # inizializzo il contatore per le classi
        Sum = 0

        # itero sul file XML cercando la chiave "name"  
        for name in root.iter('name'):
            a = name.text

            # se presente la classe di un oggetto non ancora contata, la conto
            if a in _dict and _dict[a] == 0:

                index = self.listClass.index(a)

                lines[index] = 1
                _dict[a] = _dict[a] + 1

                # aggiorno il contatore delle classi nella riga del file immagine
                Sum += 1

            else:
                pass

        lines[21] = Sum
        with open('./Notebook/1_Datasets/CSV/dataVOC.csv', 'a', newline='') as writeFile:
            # parametro "newline" evita inserimento di riga vuota

            writer = csv.writer(writeFile)
            writer.writerow(lines)  # aggiungo riga coi dati del file letto

    if __name__ == "__main__":
        main()
