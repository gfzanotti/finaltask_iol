from src.utils import *
import numpy as np
import os
import cv2
import pandas as pd
from shutil import copyfile



class Dataset:

    def main(self):
        pass

    @classmethod
    def from_input(cls):
        return cls(annotation_path=input('\n inserire percorso files delle ANNOTAZIONI : \n'),
                   images_path=input('\n inserire percorso delle IMMAGINI : \n'),
                   dest_dir=input("\n inserire percorso destinazione DATASET : \n"))

    # COSTRUTTORE
    def __init__(self, annotation_path, images_path, dest_dir):
        self.annotation_path = annotation_path
        self.images_path = images_path
        self.dest_dir = dest_dir

    # creo un dataframe dal file *.csv da cui seleziono i file con 1 sola classe
    def selCsvData(self, file_name="./Notebook/1_Datasets/CSV/dataCOCO.csv"):
        print(f' Nome File = {file_name}')
        # carico datafame da file CSV
        df = pd.read_csv(file_name)

        # SELEZIONO righe (file) con valore nella colonna Sum_Obj = 1
        # ovvero con 1 solo tipo di oggetto classificato
        df1Class = df.loc[df['Sum_Obj'] == 1]

        # TOLGO ultima colonna (somma) per andare a selezionare i file con una classe sola
        # imposto l'indice sulla prima colonna dati
        df1Class = df1Class.drop(columns=['Sum_Obj']).set_index('Image_Id')

        # codice solo per Coco
        if 'COCO' in file_name:
            # tolgo anche le colonne delle classi che non interessano
            df1Class = df1Class.filter(['Image_Id', '1', '16', '17', '18'])
            # rinomino le intestazioni di colonna per uniformarmi al dataset VOC
            df1Class = df1Class.rename(columns={"1": "bird", "16": "cat", "17": "dog", "18": "person"})

            # seleziono righe con somma >= 1 ; ovvero elimino file che non servono
            dfReduced = df1Class.loc[df1Class.sum(axis=1) >= 1]

            # memorizzo num minimo immmagini nelle classi selezionate
            _min = dfReduced.sum(axis=0).min()
        # fine codice solo per Coco

        # codice solo per VOC
        else:
            # QUANTE IMMAGINI HO PER OGNI CLASSE?
            df1Class.sum(axis=0)

            #  SCARTO le classi con poche immagini (meno di 730)
            df1Class = df1Class[df1Class.columns[df1Class.sum() > 729]]

            #  tolgo i file che non sono nelle classi principali
            dfReduced = df1Class.loc[df1Class.sum(axis=1) == 1]

            _min = 730
        # fine codice solo per VOC

        print('numero minimo di immagini tra le classi = ', _min)
        print('\n STAMPO NUMERO DI IMMAGINI PER CLASSE \n')
        print(dfReduced.sum(axis=0))

        dfReduced.sample(frac=1)  # mischio il dataframe

        # per non evidenziare il warning "SettingWithCopyWarning"
        pd.set_option('mode.chained_assignment', None)

        for cols in dfReduced:
            print('\n Classe : ', cols)
            indexNames = dfReduced[dfReduced[cols] == 1].index
            print('N° immagini iniziali : ', len(indexNames))
            indexNames = indexNames[_min:]  # # NEL CASO DI VOC MIN = 730 ALTRIMENTI min
            print('N° immagini scartate : {} \n'.format(len(indexNames)))

            # elimino dal dataframe le righe selezionate in "indexNames"
            dfReduced.drop(indexNames, inplace=True)

        return dfReduced

    def createDataset(self, dfReduced):

        # list_classes è importata da utils.py
        count = dict(zip(list_classes, (0 for x in range(len(list_classes)))))
        for image_id in dfReduced.index:

            for cat_id in dfReduced.loc[image_id].index:
                if dfReduced.loc[image_id][cat_id] == 1:
                    count[cat_id] += 1

                    if 'COCO' in self.annotation_path:
                        _fileName = self.cocoSet.imgs[image_id]['file_name']
                        #_class = self.cocoSet.cats[int(cat_id)]['name']  # [int(cat_id)]['name']
                        _class = cat_id

                    else:
                        _fileName = image_id
                        _class = cat_id

                    File_path = '/' + _class + '/' + _fileName  # .../_class/_fileName
                    srcpath = self.images_path + '/' + _fileName

                    if 'COCO' in self.annotation_path:
                        if not os.path.exists(self.dest_dir + '/ImgForClass/' + self.dir_img + '/' + _class):
                            os.makedirs(self.dest_dir + '/ImgForClass/' + self.dir_img + '/' + _class)

                        destpath = self.dest_dir + '/ImgForClass/' + self.dir_img + '/' + File_path

                    else:
                        if not os.path.exists(self.dest_dir + '/ImgForClass_TrainVal/' + _class):
                            os.makedirs(self.dest_dir + '/ImgForClass_TrainVal/' + _class)
                        if not os.path.exists(self.dest_dir + '/ImgForClass_Test/' + _class):
                            os.makedirs(self.dest_dir + '/ImgForClass_Test/' + _class)

                        if count[cat_id] > 650:  # memorizzo 80 files nella directory test
                            destpath = self.dest_dir + "/ImgForClass_Test/" + File_path
                        else:
                            destpath = self.dest_dir + "/ImgForClass_TrainVal/" + File_path

                    copyfile(srcpath, destpath)

        print(count)

    def datasetMeanVar(self):
        count = 0
        mean = [0, 0, 0]
        SD = [0, 0, 0]

        for root, dirs, files in os.walk(self.dest_dir):

            for file in files:
                if file.endswith(".jpg"):
                    count += 1
                    img = cv2.imread(os.path.join(root, file))
                    mean += np.mean(np.asarray(img), axis=(0, 1))
                    SD += np.std(np.asarray(img), axis=(0, 1))

        mean = mean / count
        SD = SD / count

        print('file processati : ', count)
        print('MEDIA = %s ' % mean)
        print('DevStd = %s ' % SD)

        return mean, SD

    if __name__ == "__main__":
        main()
