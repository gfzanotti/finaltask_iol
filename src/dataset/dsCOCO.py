from src.dataset.dataset import Dataset
from pycocotools.coco import COCO

import os
import csv


class DatasetCoco(Dataset):

    def main(self):
        pass

    # COSTRUTTORE
    def __init__(self, annotation_path, images_path, dest_dir):
        super().__init__(annotation_path, images_path, dest_dir)
        self.dir_img = os.path.basename(images_path)
        self.json_path = '/instances_' + self.dir_img + '.json'

        self.cocoSet = COCO(self.annotation_path + self.json_path)  # E:\DATAset\COCOSET + /annotations/dirImages

    # organizzo un dizionario che contiene le cassi di oggetti e numero di istanze = 0
    def dictCatName(self):
        _dict = {}
        key = 1  # uso questa chiave per match con colonne file CSV
        for k, v in self.cocoSet.cats.items():
            try:
                v.pop('supercategory')
            except KeyError:
                pass

            if 'numObj' not in v:
                v.setdefault('numObj', 0)
            _dict[key] = v
            key += 1
        return _dict

    def coco2csv(self):
        _dict = self.dictCatName()
        intestazione = []

        try:
            os.remove('./Notebook/1_Datasets/CSV/dataCOCO.csv')
        except IOError:
            print("File data.csv non cancellato: non esiste")

        with open('./Notebook/1_Datasets/CSV/dataCOCO.csv', 'a', newline='') as writeFile:
            writer = csv.writer(writeFile)

            # aggiungo la riga di intestazione
            intestazione.append('Image_Id')
            for x in range(1, 81):
                intestazione.append(_dict[x]['id'])

            intestazione.append('Sum_Obj')
            writer.writerow(intestazione)

        for image_id in self.cocoSet.imgToAnns:  # parsing dict imgToAnn per la chiave image_id
            lines = [0 for x in range(1, 82)]  # azzero le 80 celle che corrisponderebbero agli Id delle classi
            # print(type(lines[1]))    #<class 'int'>
            # print(image_id)

            lines[0] = image_id
            Sum = 0

            for annotationKey in self.cocoSet.imgToAnns[image_id]:  # leggo le key dei dict (sono valori di image_id)
                # print(annotationKey['category_id'])

                for key in _dict:
                    if annotationKey['category_id'] == _dict[key]['id']:
                        if lines[key] == 0:
                            lines[key] += 1
                            Sum += 1  # conta le classi
                        else:
                            pass
                            # lines[key] += 1    # conto tutti gli oggetti

            lines.append(Sum)
            with open('./Notebook/1_Datasets/CSV/dataCOCO.csv', 'a', newline='') as writeFile:
                writer = csv.writer(writeFile)
                writer.writerow(lines)

    if __name__ == "__main__":
        main()
