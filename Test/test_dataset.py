import unittest
from unittest import TestCase

from src.dataset.dsVOC import DatasetVoc as Ds


# import numpy as np


class TestDs(TestCase):
    def setUp(self):
        tuple_args = ('E:/DATAset/VOCdevkit/VOC2012/Annotations',
                      'E:/DATAset/VOCdevkit/VOC2012/JPEGImages',
                      './Test/fixtures/imgMeanVar')
        self.dsVoc = Ds(*tuple_args)


class TestInit(TestDs):
    def test_typeAnnoPath(self):
        self.assertEqual(self.dsVoc.annotation_path, 'E:/DATAset/VOCdevkit/VOC2012/Annotations', 'path errato')

    def test_typeAnnoPath2(self):
        self.assertIsInstance(self.dsVoc.annotation_path, str, 'tipo errato not string')

    def test_typeImgPath(self):
        self.assertIsInstance(self.dsVoc.images_path, str, 'tipo errato not string')

    def test_typeDestDir(self):
        self.assertNotEqual(self.dsVoc.dest_dir, 'E:/bbb/VOC', 'ERRORE NotEqual')

    def test_typeListClass(self):
        self.assertIsInstance(self.dsVoc.listClass, list, 'ERRORE non è una lista')

    def test_selCsvData(self):
        df_type = self.dsVoc.selCsvData("./Test/fixtures/Test.csv")
        self.assertNotIsInstance(df_type, list, 'ERRORE non è una lista')

    def test_dsMeanVar(self):
        mean, sd = self.dsVoc.datasetMeanVar()
        self.assertNotIsInstance(mean, list, 'ERRORE è una lista')


if __name__ == '__main__':
    unittest.main()
