import unittest
from unittest import TestCase

from src.heatmap.image import Image
from src.heatmap.hmCam import HeatmapCam
import numpy as np

import collections

try:
    collectionsAbc = collections.abc
except AttributeError:
    collectionsAbc = collections


class TestHeatmapCam(TestCase):
    def setUp(self):
        self.heatmapCam = HeatmapCam('E:/PROVA_VOC/ImgForClass_Test/cat/2010_004286.jpg',
                                     'E:/MODEL/VGG16/camVOC_20200818-172023_model.h5',
                                     ('global_average_pooling2d', 'dense'))
        self.img_original = self.heatmapCam.img_original
        self.heatmapResizeImg = self.heatmapCam.heatmapResizeImg


class TestInit(TestHeatmapCam):
    def test_typeImgPath_1(self):
        self.assertEqual(self.heatmapCam.img_path, 'E:/PROVA_VOC/ImgForClass_Test/cat/2010_004286.jpg', 'path errato')

    def test_typeImgPath_2(self):
        self.assertIsInstance(self.heatmapCam.img_path, str, 'tipo errato not string')

    def test_typeImageOriginal(self):
        self.assertIsInstance(self.heatmapCam.img_original, np.ndarray, 'tipo errato not ndarray')

    def test_type_image_original(self):
        self.assertNotIsInstance(self.heatmapCam.img_original, int, 'TIPO = int')

    def test_typeImage(self):
        self.assertIsInstance(self.heatmapCam.image, Image, 'tipo errato not class Image')

    def test_shapeImageOriginal_1(self):
        self.assertEqual(self.heatmapCam.img_original.shape[1], 500, 'valore errato')

    def test_shape_image_original_2(self):
        self.assertEqual(self.heatmapCam.img_original.shape[1], 175, 'valore errato')

    def test_method_img_for_cam(self):
        self.image, self.img_array = self.heatmapCam.image.img_for_cam(224, 224)
        self.assertIsInstance(self.image, np.ndarray, 'tipo errato')
        self.assertIsInstance(self.img_array, np.ndarray, 'tipo errato')

    def test_method_cam_heatmap(self):
        image, img_array = self.heatmapCam.image.img_for_cam(224, 224)
        self.heatmap = self.heatmapCam.cam_heatmap(img_array)
        self.assertIsInstance(self.heatmap, np.ndarray, 'tipo errato')

    def test_method_get_cam_for_img(self):
        image, img_array = self.heatmapCam.image.img_for_cam(224, 224)
        self.heatmapCam.cam_heatmap(img_array)
        obj = self.heatmapCam.get_cam_for_img()
        self.assertIsInstance(obj, np.ndarray, 'tipo errato')

    def test_method_overlay_heatmap(self):
        image, img_array = self.heatmapCam.image.img_for_cam(224, 224)
        self.heatmapCam.cam_heatmap(img_array)
        self.heatmapCam.get_cam_for_img()
        sth = self.heatmapCam.overlay_heatmap(0.7)
        self.assertIsInstance(sth, np.ndarray, 'tipo errato')


if __name__ == '__main__':
    unittest.main()

'''    def test_set_test_bbox_txt(self):
        self.fail()

    def test_set_gt_bbox_txt(self):
        self.fail()'''
