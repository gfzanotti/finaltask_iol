from unittest import TestCase
from src.heatmap.image import Image
import numpy as np


class TestImage(TestCase):
    def setUp(self):
        self.image = Image('E:/PROVA_VOC/ImgForClass_Test/cat/2010_004286.jpg')


class TestInit(TestImage):
    def test_get_image_copy(self):
        self.assertIsInstance(self.image.get_image_copy(), np.ndarray, 'tipo errato not ndarray')

    def test_img_for_cam(self):
        i, j = self.image.img_for_cam(224, 224)
        self.assertIsInstance(i, np.ndarray, 'tipo errato not ndarray')

    def test_get_attribute(self):
        self.assertIsInstance(self.image, Image, 'tipo errato not Image')
